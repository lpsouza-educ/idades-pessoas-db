﻿using System;
using MySql.Data.MySqlClient;

namespace IdadesPessoas
{
    class Program
    {
        static void Main(string[] args)
        {
            // Fazer um programa que leia a tabela pessoas e
            // traga o nome e a idade das 5 últimas pessoas 
            // cadastradas.

            // Lembrem-se que a connection string precisa ser ajustada para cada banco de dados! #ficaadica
            string connectionString = "server=127.0.0.1;user id=root;password=senha;port=3306;database=escola_db;";
            MySqlConnection conn = new MySqlConnection(connectionString);

            conn.Open();

            // System.Threading.Thread.Sleep(30000);

            string query = "select nome, data_nascimento from pessoas order by id desc limit 5";
            MySqlCommand command = new MySqlCommand(query, conn); // O erro da última aula foi a falta do objeto de conexão aqui no comando (tava faltando o conn).
            
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                string nome = (string) reader["nome"];
                DateTime nasc = (DateTime) reader["data_nascimento"];

                // O tema de casa é construir aqui o algoritmo que pega a data contida na variável nasc e criar uma nova variável com a idade da pessoa e colocar ali no WriteLine.

                DateTime hoje = DateTime.Now;

                int idade = hoje.Year - nasc.Year;

                if (hoje.Month < nasc.Month) {
                    idade--;
                } else if (hoje.Month == nasc.Month && hoje.Day < nasc.Day) {
                    idade--;
                }

                // if (nasc > hoje.AddYears(-idade)) idade--;

                Console.WriteLine(string.Format("{0} tem {1} anos.", nome, idade));
            }

            reader.Close();

            conn.Close();
        }
    }
}
